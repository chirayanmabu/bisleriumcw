﻿using System.ComponentModel.DataAnnotations;

namespace BIsleriumCW.Models
{
    public class Blog
    {

        [Key]
        public int BlogID { get; set; }
        [Required]
        public string BlogTitle { get; set;}
        public string BlogDescription { get; set;}

        public string BlogImageUrl { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public bool IsDeleted { get; set; }
    }
}
