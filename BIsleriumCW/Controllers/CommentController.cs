﻿using BIsleriumCW.Data;
using BIsleriumCW.Models;
using Microsoft.AspNetCore.Mvc;

namespace BIsleriumCW.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly BisleriumDbContext dbContext;

        public CommentController(BisleriumDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost]
        public async Task<IActionResult> CreateComment(Comment request)
        {
            var AddComment = new Comment()
            {
                Comments = request.Comments
            };

            await dbContext.Comments.AddAsync(AddComment);
            await dbContext.SaveChangesAsync();

            return Ok(AddComment);
        }
    }
}
