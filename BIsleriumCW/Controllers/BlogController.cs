﻿using BIsleriumCW.Data;
using BIsleriumCW.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Scaffolding.Metadata;
using System.Data;

namespace BIsleriumCW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        private readonly BisleriumDbContext _dbContext;
        private IConfiguration _configuration;

        public BlogController(BisleriumDbContext dbContext, IConfiguration configuration)
        {
            _dbContext = dbContext;
            _configuration = configuration;
        }

        //public BlogController(BisleriumDbContext dbContext)
        //{
        //    this.dbContext = dbContext;
        //}

        [HttpPost]
        public async Task<IActionResult> CreateBlog(Blog request)
        {
            var AddBlog = new Blog()
            {
                BlogTitle = request.BlogTitle,
                BlogDescription = request.BlogDescription,
                BlogImageUrl = request.BlogImageUrl
            };

            await _dbContext.Blogs.AddAsync(AddBlog);
            await _dbContext.SaveChangesAsync();

            return Ok(AddBlog);
        }

        
        //public BlogController(IConfiguration configuration)
        //{
        //    _configuration = configuration;
        //}

        [HttpGet]
        [Route("ListBlogs")]
        public JsonResult GetBlogs()
        {
            string query = "select * from dbo.Blogs";
            DataTable table = new DataTable();
            string sqlDatasource = _configuration.GetConnectionString("BisleriumConnectionString");
            SqlDataReader myReader;
            using(SqlConnection myConn = new SqlConnection(sqlDatasource))
            {
                myConn.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myConn))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myConn.Close();
                }
            }

            return new JsonResult(table);
        }
    }
}
