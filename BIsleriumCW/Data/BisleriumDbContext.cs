﻿using BIsleriumCW.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BIsleriumCW.Data
{
    public class BisleriumDbContext : IdentityDbContext
    {
        public BisleriumDbContext(DbContextOptions options) : base(options)
        { 
        
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options) =>
        options.UseSqlServer("Server=localhost;Database=BisleriumDB;Trusted_connection=True;Encrypt=False;");
        public DbSet<Blog> Blogs { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<BlogReaction> BlogReactions { get; set; }

        public DbSet<CommentReaction> CommentReactions { get; set; }
    }
}
